# OpenGauss Builtin_func的结构

要在openGauss中定义一个内置函数，需要了解Builtin_func这个结构的内容

```c++
/* This table stores all info of all builtin function. It inherits from FmgrBuiltin, but
 * unlike FmgrBuiltin, its table entries are required to be ordered by funcName, so that
 * binary search on funcName can be used when accessing the function info by function name.
 * Caution: the order of argmentes in the strunct cannot be changed!*/
typedef struct {
    Oid foid;             /* [ 0] */
    const char* funcName; /* [ 1] C name of the function or procedure name */
    int2 nargs;           /* [ 2] number of arguments, 0..FUNC_MAX_ARGS, or -1 if variable count */
    bool strict;          /* [ 3] strict with respect to NULLs? */
    bool retset;          /* [ 4] returns a set? */
    PGFunction func;      /* [ 5] pointer to compiled function */
    Oid rettype;          /* [ 6] OID of result type */
    Oid pronamespace;     /* [ 7] OID of namespace containing this proc */
    Oid proowner;         /* [ 8] procedure owner */
    Oid prolang;          /* [ 9] OID of pg_language entry, 12 for internal, 14 for SQL statement */
    float4 procost;       /* [10] estimated execution cost */
    float4 prorows;       /* [11] estimated # of rows out (if proretset) */
    Oid provariadic;      /* [12] element type of variadic array, or 0 */
    regproc protransform; /* [13] transforms calls to it during planning */
    bool proisagg;        /* [14] is it an aggregate? */
    bool proiswindow;     /* [15] is it a window function? */
    bool prosecdef;       /* [16] security definer */
    bool proleakproof;    /* [17] is it a leak-proof function? */
    char provolatile;     /* [18] see PROVOLATILE_ categories below */
    int2 pronargdefaults; /* [19] number of arguments with defaults */

    /* variable-length fields start here */
    ArrayOid proargtypes; /* [20] parameter types (excludes OUT params) */

    /* nullable fields start here, they are defined by pointer, if one of them
     * is null pointer, it means it has SQL NULL value */
    ArrayOid* proallargtypes;    /* [21] all param types (NULL if IN only) */
    ArrayChar* proargmodes;      /* [22] parameter modes (NULL if IN only) */
    ArrayCStr* proargnames;      /* [23] parameter names (NULL if no names) */
    const char* proargdefaults;  /* [24] list of expression trees for argument defaults (NULL if none) */
    const char* prosrc;          /* [25] procedure source text */
    const char* probin;          /* [26] secondary procedure info (can be NULL) */
    ArrayCStr* proconfig;        /* [27] procedure-local GUC settings */
    ArrayAcl* proacl;            /* [28] access permissions */
    ArrayInt2* prodefaultargpos; /* [29] */
    bool* fencedmode;            /* [30] */
    bool* proshippable;          /* [31] if provolatile is not 'i', proshippable will determine if the func can be shipped */
    bool* propackage;            /* [32] */
    const char* descr;           /* [33] description */
    char prokind;                /* [34] f:function, p:procedure*/
} Builtin_func;
```

0. Oid foid  很显然是函数编号，通过unused_oid这个脚本可以找到没使用的编号，Oid是无符号整数

1. const char* funcName : 函数的名称，在SQL语句中使用到的名称

2. int2 nargs: 参数的个数，int2即short，2字节整型

3. bool strict: 是否支持NULL

4. bool reset：是否返回一个集合
5. PGFuncion func：指向那个函数，这里指的是在C程序中定义的函数
6. Oid rettype:用Oid来表示返回的类型，具体的映射还不清楚
7. Oid pronamespace：用Oid表示这个函数的命名空间，具体的映射在pg_namespace.h的76行
8. Oid proowner：用Oid表示函数的所有者，具体的映射在pg_authid.h的132行
9. Oid prolang：用Oid表示函数的语言，12是内置函数(C语言)，14是SQL语句形式
10. float4 procost:对执行代价的估计，具体含义还不清楚，float4即4个字节的float，就是普通的float
11. float4 prorows：输出行数的估计，如果输出的不是集合则为0
12. Oid provariadic：用Oid表示可变数组的元素类型，或者为0，暂时不知道具体
13. regproc protransform:某种类型，不知道什么意思
14. bool proisagg：是否是聚合函数,参照SQL中的聚合函数
15. bool proiswindow：是否是窗口函数，窗口函数指的是在满足某种条件的集合下执行的特殊函数，比如经过分组后的集合就是窗口，对这个集合使用的函数就是窗口函数
16. bool prosecdef：不知道什么意思
17. bool proleakproof：是否保证防止泄露？
18. char provolatile：反映函数对参数的处理，具体参照pg_proc.h line423
19. int2 pronargdefaults：有默认值的参数个数(PGSQL中不允许有这个，这是因为C语言是没有默认值的)
20. Array Oid proargtypes:所有参数的类型，ArrayOid是一个结构体，第一个参数是count表示元素个数，第二个参数是Oid指针表示一个Oid类型的数组。不包括外部参数？？？

从21开始都是对指针的定义，如果为null则它们在SQL表中就是NULL

21. ArrayOid* proallargtypes：所有参数的类型，如果只有一个就是NULL。不太理解
22. ArrayChar* proargmodes:参数的模式
23. ArrayCStr* 参数的名字

21-23说的参数和前面的参数有哪些不同呢？

24. const char* proargdefuatls:参数的默认表达式
25. const char* prosrc:函数的名称
26. const char* probin:函数的其它信息
27. ArrayCStr* proconfig: 函数的一些本地GUC设置
28. ArrayAc1* proac1: 使用权限设置，ArrayAc1是int的数组
29. ArrayInt2* prodefaultargpos：没有注释，不知道什么意思
30.  bool* fencedmode;        
31. bool* proshippable; 
32. bool* propackage; 
33.  const char* descr;  
34. char prokind;   f是函数 p是程序

可以看出还是有一些变量的定义比较模糊，可以先参照其它的类似函数来实现。