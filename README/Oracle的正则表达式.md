# Oracle的正则表达式

**Oracle中支持的正则表达式的函数主要有下面四个**

- REGEXP_LIKE:与LIKE功能类似，LIKE操作符也可以用于搜索指定模式
- REGEXP_INSTR:与INSTR的功能类似，INSTR函数可以在一个字符串中搜索指定的字符
- REGEXP_SUBSTR:与SUBSTR的功能类似，SUBSTR根据下标可以截取字符串的内容
- REGEXP_REPLACE:与REPLACE的功能类似，将字符串替换

## regexp_like

查询value中以1开头60结束的记录

```sql
select * from fzq where value like '1____60'
select * from fzq where regexp_like(value,'1....60')
```

查询value中以1开头60结束且长度是7位并且全部是数字的记录

```sql
select * from fzq where regexp_like(value,'1[0-9]{4}60');
```

查询value中不是纯数字的记录

```sql
select * from fzq where not regexp_like(value,'^[[:digit:]]+$')
```

......

## regexp_substr

使用正则表达式来指定返回串的起点和终点

```sql
regexp_substr(source_string,pattern[,position[,occurrence[,match_parameter]]])
```

source_string:源字符串，可以是常量，也可以是某个值类型为串的列

position:从源串开始搜索的位置。默认为1

occurrence:指定源串中的第几次出现，默认值为1

match_parameter:文本量，进一步定制搜索，取值如下：

- 'i'	不区分大小写
- 'c'    区分大小写
- 'n'    允许将”.“作为通配符来匹配换行符
- 'm'   将源串视为多行。即将"^"和"$"视为行的开始或结束，如果省略该参数，源串将被看作一行来处理
- 如果取值中不属于上述，则会报错，如果指定了互相矛盾的值，将使用最后一个值
- 省略该参数时，默认区分大小写，句点不匹配换行符，源串看作一行

## regexp_instr

regexp_instr函数使用正则表达式返回搜索的起点和重点。如果没有匹配的值，则返回0.

```sql
regexp_instr(source_string,pattern[,position[,occurrence[,return_option[,match_parameter]]]])
```

## replace 和 regexp_replace

replace函数用于替换串中的某个值

```sql
replace(char,search_string[,replace_string])
```

regexp_replace是replace的增强半，支持正则表达式

```sql
regexp_replace(source_string,pattern[,replace_string[,position[,occurrence[,match_parameter]]]])
```

replace_string表示用什么来替换source_string中与pattern匹配的部分。 occurrence为非负整数，0表示所有匹配项都被替换，为正数时替换第n次匹配。