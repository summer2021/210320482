# openGauss添加函数

- 在builtin_funcs.ini中添加一个函数

```ini
AddBuiltinFunc(_0(9998), _1("abs"), _2(2), _3(true), _4(false), _5(my_abs), _6(23), _7(PG_CATALOG_NAMESPACE), _8(BOOTSTRAP_SUPERUSERID), _9(INTERNALlanguageId), _10(1), _11(0), _12(0), _13(0), _14(false), _15(false), _16(false), _17(false), _18('i'), _19(0), _20(2, 23, 23), _21(NULL), _22(NULL), _23(NULL), _24(NULL), _25("my_abs"), _26(NULL), _27(NULL), _28(NULL), _29(0), _30(false), _31(NULL), _32(false), _33(NULL), _34('f'))
    ),
```

- 在builtin.h中声明函数

```c++
//builtin.h
/*mytest function,implement in int.c*/
extern Datum my_abs(PG_FUNCTION_ARGS);
```

- 在int.cpp中定义函数

```c++
//这个函数的功能为返回两个int32较大值的绝对值
/*mytest function*/
Datum my_abs(PG_FUNCTION_ARGS)
{
    if(PG_ARGISNULL(0)||PG_ARGISNULL(1))              //判断参数是否为空
    {
        ereport(ERROR, 
            (errcode(ERRCODE_NULL_VALUE_NOT_ALLOWED), 
             errmsg("cannot specify NULL as the arguement.")));
    }

    int32 arg_1,arg_2;
    int32 result;
    arg_1 = PG_GETARG_INT32(0);
    arg_2 = PG_GETARG_INT32(1);


    
    result = arg_1 > arg_2?arg_1:arg_2;

    if(unlikely(result == PG_INT32_MIN))
    {
        ereport(ERROR,(errcode(ERRCODE_NUMERIC_VALUE_OUT_OF_RANGE),errmsg("value out of range")));
    }
    result = result<0?-result:result;
    PG_RETURN_INT32(result);
}
```

- 在pg_proc.h中添加函数

```c++
DATA(insert OID=9998(abs 11 10 12 1 0 0 0 f f f f t f i 2 0 23 "23 23" _null_ _null_ _null_ _null_ my_abs _null_ _null_ _null_ 0 f _null_ f f));
```

需要注意的是这里第一步和第四步是一样的功能，可以只使用第一步

编译的时候注意可以试一下

```shell
git clean  -df
```

