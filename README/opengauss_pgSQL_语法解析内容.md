# opengauss(pgSQL)语法解析内容

## 	调用过程

​	再exec_simple_query()中，有parsetree_list=pg_parse_query_stirng(query_string),这里query_string就是原始的查询语句。而在pg_parse_query()中，有List* raw_parsetree_list，而raw_parsetree_list=raw_parse(query_string)，在raw_parser()中调用base_yyparse()完成。

## 	gram.y分析-以简单的Select语句为例

​	gram.y是parse这个文件夹中的语法解析的内容，下面只看与exec_simple_query有关的代码

```c++
extern List* parsetree;
//这是在parse.cpp中定义的变量
```

```
stmtblock:	stmtmulti			//整个语句是一个stmtmuti,它的值会交给parsetree
			{
				pg_yyget_extra(yyscanner)->parsetree = $1;
			}
		;
stmtmulti:	stmtmulti ';' stmt		//左递归模式，stmtmulti由stmt组成
				{
					if ($3 != NULL)
					{
						if (IsA($3, List))
						{
							$$ = list_concat($1, (List*)$3);
						}
						else
						{
						$$ = lappend($1, $3);//给list加一个结点
						}
					}
					else
						$$ = $1;
				}
			| stmt
				{
					if ($1 != NULL)
					{
						if (IsA($1, List))
						{
							$$ = (List*)$1;
						}
						else
						{
						$$ = list_make1($1);//这里调用list_make1，制作一个结点吗？
						}
					}
					else
						$$ = NIL;
				}
		;
```

要注意的是list_make,它是一个宏，实际上调用的函数是

```c++
List* lcons(void* datum, List* list)//$1是第一个参数
{
    Assert(IsPointerList(list));

    if (list == NIL) {
        list = new_list(T_List);
    } else {
        new_head_cell(list);
    }

    lfirst(list->head) = datum;
    check_list_invariants(list);
    return list;
}

//又有
static List * new_list(NodeTag type)
{
   List       *new_list;
   ListCell   *new_head;

   new_head = (ListCell *) palloc(sizeof(*new_head));
   new_head->next = NULL;
   /* new_head->data is left undefined! */

   new_list = (List *) palloc(sizeof(*new_list));
   new_list->type = type;
   new_list->length = 1;
   new_list->head = new_head;
   new_list->tail = new_head;

   return new_list;
}
//即通过palloc生成一个List
```

而stmt就是以分号为分隔符的SQL语句

```
stmt :
           AlterDatabaseStmt
           | AlterDatabaseSetStmt
           | AlterDomainStmt
           | AlterFdwStmt
           | AlterForeignServerStmt
           | AlterFunctionStmt
           .......
           | SelectStmt
           .......
           | /*EMPTY*/
               { $$ = NULL; }
       ;
```

注意到简单查询语句SelectStmt也在其中（在15686行）

```
SelectStmt: select_no_parens			%prec UMINUS
			| select_with_parens		%prec UMINUS
		;

select_with_parens:
			'(' select_no_parens ')'				{ $$ = $2; }	//提取第二个值，即select_no_parens的值
			| '(' select_with_parens ')'			{ $$ = $2; }
```

这里两种语句分别表示带括号的select语句和不带括号的select语句，为了简单，看不带括号的语句

```
select_no_parens:
			simple_select						{ $$ = $1; }
			| select_clause sort_clause
				{
					insertSelectOptions((SelectStmt *) $1, $2, NIL,
										NULL, NULL, NULL,
										yyscanner);
					$$ = $1;
				}
			| select_clause opt_sort_clause for_locking_clause opt_select_limit
				{
					insertSelectOptions((SelectStmt *) $1, $2, $3,
										(Node*)list_nth($4, 0), (Node*)list_nth($4, 1),
										NULL,
										yyscanner);
					$$ = $1;
				}
			| select_clause opt_sort_clause select_limit opt_for_locking_clause
				{
					insertSelectOptions((SelectStmt *) $1, $2, $4,
										(Node*)list_nth($3, 0), (Node*)list_nth($3, 1),
										NULL,
										yyscanner);
					$$ = $1;
				}
			| with_clause select_clause
				{
					insertSelectOptions((SelectStmt *) $2, NULL, NIL,
										NULL, NULL,
										$1,
										yyscanner);
					$$ = $2;
				}
			| with_clause select_clause sort_clause
				{
					insertSelectOptions((SelectStmt *) $2, $3, NIL,
										NULL, NULL,
										$1,
										yyscanner);
					$$ = $2;
				}
			| with_clause select_clause opt_sort_clause for_locking_clause opt_select_limit
				{
					insertSelectOptions((SelectStmt *) $2, $3, $4,
										(Node*)list_nth($5, 0), (Node*)list_nth($5, 1),
										$1,
										yyscanner);
					$$ = $2;
				}
			| with_clause select_clause opt_sort_clause select_limit opt_for_locking_clause
				{
					insertSelectOptions((SelectStmt *) $2, $3, $5,
										(Node*)list_nth($4, 0), (Node*)list_nth($4, 1),
										$1,
										yyscanner);
					$$ = $2;
				}
		;

select_clause:
			simple_select							{ $$ = $1; }
			| select_with_parens					{ $$ = $1; }
		;

```

其中先来看simple_select:

```
simple_select:
			SELECT hint_string opt_distinct target_list
			into_clause from_clause where_clause
			group_clause having_clause window_clause
				{
					SelectStmt *n = makeNode(SelectStmt);
					n->distinctClause = $3;
					n->targetList = $4;
					n->intoClause = $5;
					n->fromClause = $6;
					n->whereClause = $7;
					n->groupClause = $8;
					n->havingClause = $9;
					n->windowClause = $10;
					n->hintState = create_hintstate($2);
					n->hasPlus = getOperatorPlusFlag();
					$$ = (Node *)n;
				}
				......
```

关于这个地方，先来看SelectStmt的数据结构,其中List是链表，比如fromClause有多个来源，所以要用链表来存，而Node中包含一个enum结构NodeTag

```c++
typedef struct SelectStmt {
    NodeTag type;

    /*
     * These fields are used only in "leaf" SelectStmts.
     */
    List *distinctClause;   /* NULL, list of DISTINCT ON exprs, or
                             * lcons(NIL,NIL) for all (SELECT DISTINCT) */
    IntoClause *intoClause; /* target for SELECT INTO */
    List *targetList;       /* the target list (of ResTarget) */
    List *fromClause;       /* the FROM clause */
    Node *whereClause;      /* WHERE qualification */
    List *groupClause;      /* GROUP BY clauses */
    Node *havingClause;     /* HAVING conditional-expression */
    List *windowClause;     /* WINDOW window_name AS (...), ... */
    WithClause *withClause; /* WITH clause */

    /*
     * In a "leaf" node representing a VALUES list, the above fields are all
     * null, and instead this field is set.  Note that the elements of the
     * sublists are just expressions, without ResTarget decoration. Also note
     * that a list element can be DEFAULT (represented as a SetToDefault
     * node), regardless of the context of the VALUES list. It's up to parse
     * analysis to reject that where not valid.
     */
    List *valuesLists; /* untransformed list of expression lists */

    /*
     * These fields are used in both "leaf" SelectStmts and upper-level
     * SelectStmts.
     */
    List *sortClause;    /* sort clause (a list of SortBy's) */
    Node *limitOffset;   /* # of result tuples to skip */
    Node *limitCount;    /* # of result tuples to return */
    List *lockingClause; /* FOR UPDATE (list of LockingClause's) */
    HintState *hintState;

    /*
     * These fields are used only in upper-level SelectStmts.
     */
    SetOperation op;         /* type of set op */
    bool all;                /* ALL specified? */
    struct SelectStmt *larg; /* left child */
    struct SelectStmt *rarg; /* right child */

    /*
     * These fields are used by operator "(+)"
     */
    bool hasPlus;
    /* Eventually add fields for CORRESPONDING spec here */
} SelectStmt;
```

![image-20210717090921250](C:\Users\HP\AppData\Roaming\Typora\typora-user-images\image-20210717090921250.png)

这样，一个简单的查询语句形成一个List，List中只有一个结点，这个结点存储了一个SelectStmt结构，根据这个结构可以在磁盘中查找数据。Node大概是根据每个类型不同造出来的结构也不相同，但是每个Node都必须带有开头的enum结构

那么下面来具体分析这个结构

### target_list分析

```
target_list:
           target_el                                { $$ = list_make1($1); }
           | target_list ',' target_el                { $$ = lappend($1, $3); }
       ;

target_el:    a_expr AS ColLabel
               {
                   $$ = makeNode(ResTarget);		//ResTarget应该是这个Node的类型
                   $$->name = $3;
                   $$->indirection = NIL;
                   $$->val = (Node *)$1;
                   $$->location = @1;
               }
           /*
           ...
           | a_expr
               {
                   $$ = makeNode(ResTarget);
                   $$->name = NULL;
                   $$->indirection = NIL;
                   $$->val = (Node *)$1;
                   $$->location = @1;
               }
           | '*'
               {
                   ColumnRef *n = makeNode(ColumnRef);
                   n->fields = list_make1(makeNode(A_Star));
                   n->location = @1;

                   $$ = makeNode(ResTarget);
                   $$->name = NULL;
                   $$->indirection = NIL;
                   $$->val = (Node *)n;
                   $$->location = @1;
               }
       ;
```

根部分是一个典型的左递归结构，如果只有一个，则创建一个list，否则将数据append到已有的list当中去。而后面的target_el就是查找字段的表示形式：a_expr AS ColLabel或a_expr，把它的数据存储到一个Node结构之中

下面再来看a_expr:

```
a_expr:		c_expr									{ $$ = $1; }
			| a_expr TYPECAST Typename
					{ $$ = makeTypeCast($1, $3, @2); }
			| a_expr COLLATE any_name
				{
					CollateClause *n = makeNode(CollateClause);
					n->arg = $1;
					n->collname = $3;
					n->location = @2;
					$$ = (Node *) n;
				}
				......
				
c_expr:		columnref								{ $$ = $1; }
			| AexprConst							{ $$ = $1; }
```

后面的内容太多，先顺着c_expr中的columnref走下去：

```
columnref:	ColId
				{
					$$ = makeColumnRef($1, NIL, @1, yyscanner);
				}
			| ColId indirection
				{
					$$ = makeColumnRef($1, $2, @1, yyscanner);
				}
			| EXCLUDED indirection
				{
					$$ = makeColumnRef("excluded", $2, @2, yyscanner);
				}
		;
......
ColId:		IDENT									{ $$ = $1; }
			| unreserved_keyword					{ $$ = pstrdup($1); }
			| col_name_keyword						{ $$ = pstrdup($1); }
```

最终我们找到了IDENT，而IDENT可以在词法分析文件scan.I中找到，它是一个直接被返回的token

```
{identifier}	{
					const ScanKeyword *keyword;
					char		   *ident;

					SET_YYLLOC();

					/* Is it a keyword? */
					keyword = ScanKeywordLookup(yytext,
												yyextra->keywords,
												yyextra->num_keywords);

					yyextra->is_hint_str = false;

					if (keyword != NULL)
					{
						yylval->keyword = keyword->name;

						/* Find the CREATE PROCEDURE syntax and set dolqstart. */
						if (keyword->value == CREATE)
						{
							yyextra->is_createstmt = true;
						}
						else if (keyword->value == TRIGGER && yyextra->is_createstmt)
						{
							/* Create trigger don't need set dolqstart */
							yyextra->is_createstmt = false;
						}
						else if ((keyword->value == PROCEDURE || keyword->value == FUNCTION)
								 && yyextra->is_createstmt)
						{
							/* Make yyextra->dolqstart not NULL means its in a proc with $$. */
							yyextra->dolqstart = "";
						}
						else if (keyword->value == BEGIN_P)
						{
							/* cases that have to be a trans stmt and fall quickly */
							if (yyg->yy_hold_char == ';' || /* found ';' after 'begin' */
								yyg->yy_hold_char == '\0')  /* found '\0' after 'begin' */
								return BEGIN_NON_ANOYBLOCK;
							/* look for other transaction stmt */
							if (is_trans_stmt(yyextra->scanbuf, yyextra->scanbuflen))
								return BEGIN_NON_ANOYBLOCK;
						}
						else if (keyword->value == SELECT ||
								 keyword->value == UPDATE||
								 keyword->value == INSERT ||
								 keyword->value == DELETE_P ||
								 keyword->value == MERGE)
						{
							yyextra->is_hint_str = true;
						}

						return keyword->value;
					}

					/*
					 * No.  Convert the identifier to lower case, and truncate
					 * if necessary.
					 */
					ident = downcase_truncate_identifier(yytext, yyleng, yyextra->warnOnTruncateIdent);
					yylval->str = ident;
					yyextra->ident_quoted = false;
					return IDENT;
				}
```

即select语句后面的字段名称会被词法分析识别成IDENT这个token。也就是有这样一个路线

IDENT->ColID->columnref->c_expr->a_expr->target_list

这里第一步只是简单的参数传递，而第二步返回的结构则有所不同，根据代码可以看出，columnref带回的值是：makeColumnRef($1, NIL, @1, yyscanner);这个函数的返回值，那么我们此时去追溯这个函数：

```c++
typedef struct ColumnRef {
    NodeTag type;
    List *fields; /* field names (Value strings) or A_Star */
    int location; /* token location, or -1 if unknown */
} ColumnRef;    //ColumnRef的结构，第一个是类型，第二个是字段的名称，第三个是token的位置

//这是gram.y中自带的版本，此外还有一个版本，不知道有什么不同
static Node *	//ColID(也就是字段名)   NULL   1的地址    一个空类型指针
makeColumnRef(char *colname, List *indirection,
			  int location, core_yyscan_t yyscanner)
{
	/*
	 * Generate a ColumnRef node, with an A_Indirection node added if there
	 * is any subscripting in the specified indirection list.  However,
	 * any field selection at the start of the indirection list must be
	 * transposed into the "fields" part of the ColumnRef node.
	 */
	ColumnRef  *c = makeNode(ColumnRef);		//获得一个Node
	int		nfields = 0;				
	ListCell *l;							//List结点

	c->location = location;					//location赋值
	foreach(l, indirection)				//遍历List(单个的字段读进来这里是NULL)
	{
		if (IsA(lfirst(l), A_Indices))
		{
			A_Indirection *i = makeNode(A_Indirection);

			if (nfields == 0)
			{
				/* easy case - all indirection goes to A_Indirection */
				c->fields = list_make1(makeString(colname));
				i->indirection = check_indirection(indirection, yyscanner);
			}
			else
			{
				/* got to split the list in two */
				i->indirection = check_indirection(list_copy_tail(indirection,
																  nfields),
												   yyscanner);
				indirection = list_truncate(indirection, nfields);
				c->fields = lcons(makeString(colname), indirection);
			}
			i->arg = (Node *) c;
			return (Node *) i;
		}
		else if (IsA(lfirst(l), A_Star))
		{
			/* We only allow '*' at the end of a ColumnRef */
			if (lnext(l) != NULL)
				parser_yyerror("improper use of \"*\"");
		}
		else if (IsA(lfirst(l), String) && strncmp(strVal(lfirst(l)), "(+)", 3) == 0)
		{
			u_sess->parser_cxt.stmt_contains_operator_plus = true;
		}
		nfields++;
	}
	/* No subscripting, so all indirection gets added to field list */
	c->fields = lcons(makeString(colname), indirection);		//获得名字
	return (Node *) c;							//返回Node
}
```

接着之后的解析过程又都是简单的参数传递，最后到target_list会生成一个List以记录不同的字段(多个字段)

而targetList指向的数据部分是一个名为ResTarget的结构

```c
typedef struct ResTarget {
    NodeTag type;
    char *name;        /* column name or NULL */
    List *indirection; /* subscripts, field names, and '*', or NIL */
    Node *val;         /* the value expression to compute or assign 这里是实际的字段名*/
    int location;      /* token location, or -1 if unknown */
} ResTarget;
```





### from子句分析

```
from_clause:
			FROM from_list							{ $$ = $2; }
			| /*EMPTY*/								{ $$ = NIL; }
		;

from_list:
			table_ref								{ $$ = list_make1($1); }
			| from_list ',' table_ref				{ $$ = lappend($1, $3); }
		;
```

这里很简单，from子句为空或者FROM+from_list.而from_list结构是左递归，由多个table_ref组成。接下来是table_ref:

```
table_ref:	relation_expr
				{
					$$ = (Node *) $1;
				}
			| relation_expr alias_clause
				{
					$1->alias = $2;
					$$ = (Node *) $1;
				}
				......
```

后面都是relation_expr的递归或者funtable，所以先看relation_expr:

```
relation_expr:
			qualified_name
				{
					/* default inheritance */
					$$ = $1;
					$$->inhOpt = INH_DEFAULT;
					$$->alias = NULL;
				}
			| qualified_name '*'
				{
					/* inheritance query */
					$$ = $1;
					$$->inhOpt = INH_YES;
					$$->alias = NULL;
				}
			| ONLY qualified_name
				{
					/* no inheritance */
					$$ = $2;
					$$->inhOpt = INH_NO;
					$$->alias = NULL;
				}
				....
				
qualified_name:
			ColId
				{
					$$ = makeRangeVar(NULL, $1, @1);
				}
			| ColId indirection
				{
					check_qualified_name($2, yyscanner);
					$$ = makeRangeVar(NULL, NULL, @1);
					switch (list_length($2))
					{
						case 1:
							$$->catalogname = NULL;
							$$->schemaname = $1;
							$$->relname = strVal(linitial($2));
							break;
						case 2:
							$$->catalogname = $1;
							$$->schemaname = strVal(linitial($2));
							$$->relname = strVal(lsecond($2));
							break;
						default:
							ereport(ERROR,
									(errcode(ERRCODE_SYNTAX_ERROR),
									 errmsg("improper qualified name (too many dotted names): %s",
											NameListToString(lcons(makeString($1), $2))),
									 parser_errposition(@1)));
							break;
					}
				}
		;

```

可以看到最后又指向了ColId的内容了