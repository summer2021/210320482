# pgsql queryTree生成

在语法解析篇章中生成了简单的parsetree结构，这是一个list结构，List中存放的是SelectStmt语句，这些语句中根据类型存放着语句的条件，比如目标子句、字段子句、排列子句等。

那么querytree在内存中是什么样子的呢？假设有如下数据表

```sql
CREATE TABEL TEST (
	ID int PRIMARY KEY,
    NAME VARCHAR(10)
);
```

现在发送sql请求

```sql
select name from test;
```

当程序执行完parsetree = pg_parse_query(query_string) 后 (query_string就是实际的查询语句)，接下来要执行querytree_list=pg_analyze_and_rewrite(parsetree,query_string,NULL,0)

在此之前需要了解querytree在系统内存中是什么样的http://www.sciencenet.cn/m/user_content.aspx?id=309933

首先调用的是这个函数

```c
Query *parse_analyze(Node *parseTree, const char *sourceText, Oid *paramTypes, int numParams){
   ParseState *pstate = make_parsestate(NULL);
   Query       *query;
   Assert(sourceText != NULL); /* required as of 8.4 */
   ...
   query = transformStmt(pstate, parseTree);
   free_parsestate(pstate);
   return query;
}
```

首先是Query

```c
typedef struct Query {
    NodeTag type;

    CmdType commandType; /* select|insert|update|delete|merge|utility */

    QuerySource querySource; /* where did I come from? */

    uint64 queryId; /* query identifier (can be set by plugins) */

    bool canSetTag; /* do I set the command result tag? */

    Node* utilityStmt; /* non-null if this is DECLARE CURSOR or a
                        * non-optimizable statement */
...
}
```

可以看出无论什么Sql语句都要形成一个Query，此外，Query中也含义SelectStmt中有的targetList、等字段。

接着来看下ParseState结构：

```c
struct ParseState {
    struct ParseState* parentParseState; /* stack link */
    const char* p_sourcetext;            /* source text, or NULL if not available */
    List* p_rtable;                      /* range table so far */
    List* p_joinexprs;                   /* JoinExprs for RTE_JOIN p_rtable entries */
    List* p_joinlist;                    /* join items so far (will become FromExpr
                                            node's fromlist) */
    List* p_relnamespace;                /* current namespace for relations */
    List* p_varnamespace;                /* current namespace for columns */
    bool  p_lateral_active;              /* p_lateral_only items visible? */
    List* p_ctenamespace;                /* current namespace for common table exprs */
    List* p_future_ctes;                 /* common table exprs not yet in namespace */
    CommonTableExpr* p_parent_cte;       /* this query's containing CTE */
    List* p_windowdefs;                  /* raw representations of window clauses */
    ParseExprKind p_expr_kind;           /* what kind of expression we're parsing */
    List* p_rawdefaultlist;              /* raw default list */
    int p_next_resno;                    /* next targetlist resno to assign */
    List* p_locking_clause;              /* raw FOR UPDATE/FOR SHARE info */
    Node* p_value_substitute;            /* what to replace VALUE with, if any */
    bool p_hasAggs;
    bool p_hasWindowFuncs;
    bool p_hasSubLinks;
    bool p_hasModifyingCTE;
    bool p_is_insert;
    bool p_locked_from_parent;
    bool p_resolve_unknowns; /* resolve unknown-type SELECT outputs as type text */
    bool p_hasSynonyms;
    Relation p_target_relation;
    RangeTblEntry* p_target_rangetblentry;
    ...
}
```

这个结构主要用于记录生成的Querytree的状态。其中RangeTbleEntry结构值得注意。首先有

> query = transformStmt(pstate, parseTree);

这里我们的sql语句是select语句，执行类型判断之后会调用transformSelectStmt，在parseTree中存放了很多子句的内容，比如FromClause，每一个子句都需要调用不同的函数，这里以  

> transformFromClause(pstate, stmt->fromClause);

为例。对于每一个from子句，内容都存储在Node* n中，对每一个node，执行

> n = transformFromClauseItem(pstate, n, &rte, &rtindex,&relnamespace, &containedRels);

并对pastate中的状态赋值。具体到这个函数又调用

> rte = transformTableEntry(pstate, rv);

来执行具体的转换工作。该函数中再调用

> RangeTblEntry *rte=addRangeTableEntry(pstate, r, r->alias,interpretInhOption(r->inhOpt), true);

这样就构造出了刚刚提到的结构RangeTblEntry，这里面主要调用了

> rel = parserOpenTable(pstate, relation, lockmode);

函数。该函数在parser阶段打开一个relation。这里后面的内容暂时不进行讨论。。。



这个函数最终返回的就是Query为结点的List，即将读上来的Stmt转换类型，Query可以理解为查询语法树结构，它最终需要通过优化形成plantree结构