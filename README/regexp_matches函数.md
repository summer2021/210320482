# regexp_matches函数

在openGauss中有一个和正则表达式相关的函数：

```c++
Datum regexp_matches(PG_FUNCTION_ARGS);
```

经过测试，该函数的功能是找到能匹配正则表达式第一个子串(只给两个参数的情况)。通过分析这个函数，可能可以弄清楚regex接口的使用方法

```c++
Datum regexp_matches(PG_FUNCTION_ARGS)          //找正则匹配的子串
{
    FuncCallContext* funcctx = NULL;			//这个结构体用于存放函数的一些上下文信息，帮助设置返回值，此struct适用于函数需要返回多行时使用
    regexp_matches_ctx* matchctx = NULL;		//用于存储一些数据，例如原始字符串、匹配的子串数量等

    if (SRF_IS_FIRSTCALL()) {//如果是第一次调用这个函数
        text* pattern = PG_GETARG_TEXT_PP(1);	//获取正则表达式
        text* flags = PG_GETARG_TEXT_PP_IF_EXISTS(2);//获取其它选项，这里只研究两个参数的情况，所以用不上
        MemoryContext oldcontext;		//内存上下文信息，不关注

        funcctx = SRF_FIRSTCALL_INIT();		//初始化funcctx，比如访问数置为0
        oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

        /* be sure to copy the input string into the multi-call ctx */
        matchctx =
            setup_regexp_matches(PG_GETARG_TEXT_P_COPY(0), pattern, flags, PG_GET_COLLATION(), false, true, false);//需要调用这个函数，见1

        /* Pre-create workspace that build_regexp_matches_result needs */
        matchctx->elems = (Datum*)palloc(sizeof(Datum) * matchctx->npatterns);
        matchctx->nulls = (bool*)palloc(sizeof(bool) * matchctx->npatterns);

        MemoryContextSwitchTo(oldcontext);
        funcctx->user_fctx = (void*)matchctx;
    }

    funcctx = SRF_PERCALL_SETUP();
    matchctx = (regexp_matches_ctx*)funcctx->user_fctx;

    if (matchctx->next_match < matchctx->nmatches) {
        ArrayType* result_ary = NULL;

        result_ary = build_regexp_matches_result(matchctx);
        matchctx->next_match++;
        SRF_RETURN_NEXT(funcctx, PointerGetDatum(result_ary));
    }

    /* release space in multi-call ctx to avoid intraquery memory leak */
    cleanup_regexp_matches(matchctx);

    SRF_RETURN_DONE(funcctx);
}
```

1. static regexp_matches_ctx* setup_regexp_matches(text* orig_str, text* pattern, text* flags, Oid collation,bool force_glob, bool use_subpatterns, bool ignore_degenerate)

   这个函数的注释指出，它为regexp_matches()函数做一些初始化工作，返回值的结构中包含了所有模式匹配的子字符串的位置。这三个bool型变量只有两种形式。正则表达式子模式表示用圆括号括起来的表达式，它看作一个整体，比如(abc)+可以表示任意多个abc。其它两个还不清楚

```c++
//struct regexp_matches_ctx
typedef struct regexp_matches_ctx {
    text* orig_str; /* 原始字符串（待匹配的） */
    int nmatches;   /* 有几处子串 */
    int npatterns;  /* 有几处子模式 */
    /* We store start char index and end+1 char index for each match */
    /* so the number of entries in match_locs is nmatches * npatterns * 2 */
    //翻译：我们以左闭右开的方式存储每一组匹配的子串
    int* match_locs; /* 0-based character indexes */
    int next_match;  /* 0-based index of next match to process */
    /* workspace for build_regexp_matches_result() */
    Datum* elems; /* has npatterns elements */
    bool* nulls;  /* has npatterns elements */
} regexp_matches_ctx;

//函数体
static regexp_matches_ctx* setup_regexp_matches(text* orig_str, text* pattern, text* flags, Oid collation,
    bool force_glob, bool use_subpatterns, bool ignore_degenerate)
{
    regexp_matches_ctx* matchctx = (regexp_matches_ctx*)palloc0(sizeof(regexp_matches_ctx));//首先分配空间
    int orig_len;			//待匹配字符串的长度？
    pg_wchar* wide_str = NULL;	//用宽字符存储字符串？
    int wide_len;			//宽字符字符串的长度？
    pg_re_flags re_flags;		//这是regex_match函数中的一些选项
    regex_t* cpattern = NULL;	//regex_t结构
    regmatch_t* pmatch = NULL;	//regmatch_t,包含一组首、尾下标
    int pmatch_len;				//匹配串长度？
    int array_len;				//？
    int array_idx;				//？
    int prev_match_end;			//？		
    int start_search;			//从哪开始搜索

    /* save original string --- we'll extract result substrings from it */
    matchctx->orig_str = orig_str;	//先存放好原始的字符串

    /* convert string to pg_wchar form for matching */
    orig_len = VARSIZE_ANY_EXHDR(orig_str);//求源串长度
    wide_str = (pg_wchar*)palloc(sizeof(pg_wchar) * (orig_len + 1));	//分配一个宽字符串的空间
    wide_len = pg_mb2wchar_with_len(VARDATA_ANY(orig_str), wide_str, orig_len);
//求出对应宽字符串的长度
    /* determine options */
    parse_re_flags(&re_flags, flags);		//解析flag的信息
    if (force_glob) {
        /* user mustn't specify 'g' for regexp_split */
        if (re_flags.glob)
            ereport(ERROR,
                (errcode(ERRCODE_INVALID_PARAMETER_VALUE), errmsg("regexp_split does not support the global option")));
        /* but we find all the matches anyway */
        re_flags.glob = true;
    }

    /* set up the compiled pattern */
    cpattern = RE_compile_and_cache(pattern, re_flags.cflags, collation);
	//编译正则串，传入正则表达式字符串和选项，这个函数首先会在缓存中找是不是字符串不久前被编译过，如果是那么就直接返回。否则要调用regex接口中的re_regcomp()，由于这里涉及到re_regcomp()所以不再深入。从这个函数的处理方法中我们可以猜测，处理好的正则表达式是放在一个固定的结构u_sess->cache_cxt->re_array中，而re_array[0]就是当前处理好的正则表达式。并且re_regcomp()这个函数确实是将处理好的正则表达式放在它的第一个参数当中
    /* do we want to remember subpatterns? */
    if (use_subpatterns && cpattern->re_nsub > 0) {//使用子模式的情况
        matchctx->npatterns = cpattern->re_nsub;
        pmatch_len = cpattern->re_nsub + 1;		//子模式数+1？什么意思
    } else {
        use_subpatterns = false;
        matchctx->npatterns = 1;
        pmatch_len = 1;
    }

    /* temporary output space for RE package */
    pmatch = (regmatch_t*)palloc(sizeof(regmatch_t) * pmatch_len);
//这是前面提到的存储首尾字符的那个序列，从这里看来，分配的个数是pmatch_len，即子模式个数+1，可能是要存储上所有的匹配的位置，因为有一个子模式就表示要多匹配一个串，为什么要再+1呢？
    /* the real output space (grown dynamically if needed) */
    array_len = re_flags.glob ? 256 : 32;		//根据选项选择array_len的大小，如果有g选项则设为256，因为返回的是一个数组，要制定数组的长度
    matchctx->match_locs = (int*)palloc(sizeof(int) * array_len);//这个和array_len有关？总之是存放整数
    array_idx = 0;

    /* search for the pattern, perhaps repeatedly *///开始查找匹配串
    prev_match_end = 0;
    start_search = 0;
    while (RE_wchar_execute(cpattern, wide_str, wide_len, start_search, pmatch_len, pmatch)) {//执行匹配，这里见2
        //要注意这里是一个while语句，也就是说如果没有成功匹配就直接跳出，如果成功匹配了就去
        //找下一个。这里根据调试可以看出，返回的pmatch中带有匹配数组的下标范围。同样地，也可以理解pg_regexec()函数的执行结果了。
        /*
         * If requested, ignore degenerate matches, which are zero-length
         * matches occurring at the start or end of a string or just after a
         * previous match.
         从这里就可以看出，bool degenerate这个变量的含义是：是否忽略长度为0的匹配
         */
        if (!ignore_degenerate || (pmatch[0].rm_so < wide_len && pmatch[0].rm_eo > prev_match_end)) {//如果不忽略长度为0的情况，或找到了新的匹配串，那么继续扩充
            /* enlarge output space if needed */
            while (array_idx + matchctx->npatterns * 2 > array_len) {
                array_len *= 2;
                matchctx->match_locs = (int*)repalloc(matchctx->match_locs, sizeof(int) * array_len);
            }//如果需要就扩充空间

            /* save this match's locations */
            if (use_subpatterns) {
                int i;

                for (i = 1; i <= matchctx->npatterns; i++) {
                    matchctx->match_locs[array_idx++] = pmatch[i].rm_so;
                    matchctx->match_locs[array_idx++] = pmatch[i].rm_eo;
                }//如果使用子模式匹配，把每个子模式匹配到的都存起来
            } else {
                matchctx->match_locs[array_idx++] = pmatch[0].rm_so;
                matchctx->match_locs[array_idx++] = pmatch[0].rm_eo;
            }
            matchctx->nmatches++;
        }
        prev_match_end = pmatch[0].rm_eo;  

        /* if not glob, stop after one match */
        if (!re_flags.glob)  //如果没加g参数，只匹配第一次就好
            break;

        /*
         * Advance search position.  Normally we start the next search at the
         * end of the previous match; but if the match was of zero length, we
         * have to advance by one character, or we'd just find the same match
         * again.
         */
        start_search = prev_match_end;		//重置起点
        if (pmatch[0].rm_so == pmatch[0].rm_eo)//起点和终点相同(空匹配？)则递增
            start_search++;
        if (start_search > wide_len)
            break;
    }

    /* Clean up temp storage */
    pfree_ext(wide_str);
    pfree_ext(pmatch);

    return matchctx;			//返回匹配的结果
}
```

2. static bool RE_wchar_execute（ regex_t* re, pg_wchar* data, int data_len, int start_search, int nmatch, regmatch_t* pmatch)

   这个函数的注释指出，它会执行正则表达式和一个宽字符串的匹配，如果匹配成功返回True，匹配失败返回false

   ```c++
   static bool RE_wchar_execute(
       regex_t* re, pg_wchar* data, int data_len, int start_search, int nmatch, regmatch_t* pmatch)
   {  //编译后的正则    宽字符串       长度            起始下标            下标组数
       //存储下标
       int regexec_result;				
       char errMsg[100];
   
       /* Perform RE match and return result */
       regexec_result = pg_regexec(re,			//执行结果通过pg_regexec()返回
           data,
           data_len,
           start_search,
           NULL, /* no details */
           nmatch,
           pmatch,
           0);
   
       if (regexec_result != REG_OKAY && regexec_result != REG_NOMATCH) {
           /* re failed??? */
           pg_regerror(regexec_result, re, errMsg, sizeof(errMsg));
           ereport(ERROR, (errcode(ERRCODE_INVALID_REGULAR_EXPRESSION), errmsg("regular expression failed: %s", errMsg)));
       }
   
       return (regexec_result == REG_OKAY);//得到结果返回是否成功匹配
   }
   ```

   

   
