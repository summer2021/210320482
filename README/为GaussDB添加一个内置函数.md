# 为GaussDB添加一个内置函数

参考了[如何为PostgreSQL创建一个内置函数？ · 小wing的驿站 (xiaowing.github.io)](https://xiaowing.github.io/post/20170903_howto_create_a_postgres_builtin_function/)

由于GaussDB的内核基本沿用了PostgreSQL，所以参考这篇给PgSQL添加函数的步骤。

## 函数实现

首先是函数的实现，内置函数必须采用这样的实现：即返回值为Datum，根据操作系统的不同定义为32位无符号int或64位无符号int。参数是PG_FUNCTION_ARGS，它是一个宏定义，实际是一个结构体，后面会详细展开。首先来看测试的实现函数

```c++
Datum add_str(PG_FUNCTION_ARGS) {
    int arg_1, arg_2;
    char buf[128] = {0x00};
    char *result = NULL;

    if (PG_ARGISNULL(0) || PG_ARGISNULL(1)) {
        ereport(ERROR, 
            (errcode(ERRCODE_NULL_VALUE_NOT_ALLOWED), 
             errmsg("cannot specify NULL as the arguement.")));
    }

    arg_1 = PG_GETARG_INT32(0);
    arg_2 = PG_GETARG_INT32(1);

    snprintf(buf, 128, "%d + %d = %d", arg_1, arg_2, (arg_1 + arg_2));
    result = pstrdup(buf);

    PG_RETURN_TEXT_P(cstring_to_text(result));
}
//这是一个简单的两数相加的函数，最终返回相加的结果字符串
```

下面详细来看函数中的内容，Datum是无符号整数，PG_FUNCTION_ARGS是一个宏定义：

```c++
//fmgr_comp.h
#define PG_FUNCTION_ARGS FunctionCallInfo fcinfo


//fmgr_core.h
typedef struct FunctionCallInfoData* FunctionCallInfo;

//fmgr.h
typedef struct FunctionCallInfoData {
    FmgrInfo* flinfo;                            /* ptr to lookup info used for this call */
    fmNodePtr context;                           /* pass info about context of call */
    fmNodePtr resultinfo;                        /* pass or return extra info about result */
    Oid fncollation;                             /* collation for function to use */
    bool isnull;                                 /* function must set true if result is NULL */
    short nargs;                                 /* # arguments actually passed */
    Datum* arg;                                  /* Arguments passed to function */
    bool* argnull;                               /* T if arg[i] is actually NULL */
    Oid* argTypes;                               /* Argument type */
    Datum prealloc_arg[FUNC_PREALLOCED_ARGS];    /* prealloced arguments.*/
    bool prealloc_argnull[FUNC_PREALLOCED_ARGS]; /* prealloced argument null flags.*/
    Oid prealloc_argTypes[FUNC_PREALLOCED_ARGS]; /* prealloced argument type */
    ScalarVector* argVector;                     /*Scalar Vector */
    RefcusorInfoData refcursor_data;
    UDFInfoType udfInfo;

    FunctionCallInfoData()
    {
        flinfo = NULL;
        arg = NULL;
        argnull = NULL;
        argTypes = NULL;
        argVector = NULL;
        fncollation = 0;
        context = NULL;
        resultinfo = NULL;
        nargs = 0;
        isnull = false;
    }
} FunctionCallInfoData;
//可以看到该结构是一个标准化的参数列表，其中包含了很多函数调用了信息。
```

PG_GET_INT32()也是一个宏定义，用于获取参数

```c++
//fmgr_comp.h
#define PG_GETARG_INT32(n) DatumGetInt32(PG_GETARG_DATUM(n))

#define PG_GETARG_DATUM(n) (fcinfo->arg[n])//获取第n个参数

//postgres.h
#define DatumGetInt32(X) ((int32)GET_4_BYTES(X))
#define GET_4_BYTES(datum) (((Datum)(datum)) & 0xffffffff)
//获取变量的低32位并转换成int类型

//综上PG_GET_INT32就是将参数以int类型取出
```

最后再来看PG_RETURN_TEXT_P(cstring_to_text(result));这条语句

```c++
//fmgr_comp.h
#define PG_RETURN_TEXT_P(x) PG_RETURN_POINTER(x)
#define PG_RETURN_POINTER(x) return PointerGetDatum(x)
#define PointerGetDatum(X) ((Datum)(X))			
//即将变量转换位Datum


//varlena.cpp
text* cstring_to_text(const char* s)
{
    return cstring_to_text_with_len(s, strlen(s));
}


//text是一个结构体，里面包含了两个char型数组，大致记录的是它的长度和内容
text* cstring_to_text_with_len(const char* s, size_t len)
{
    text* result = (text*)palloc0(len + VARHDRSZ);//VARHDRSZ=4个字节
//即生成的result长度字段为4B，数据字段大小为原本的长度
    SET_VARSIZE(result, len + VARHDRSZ);		//这是一个枚举类型，实际与text相似
    if (len > 0) {
        int rc = memcpy_s(VARDATA(result), len, s, len);
        securec_check(rc, "\0", "\0");//检查是否转移成功
    }
    return result;
}
```



要注意函数可以实现在PG的任何一个代码文件中，但必须确保这个源文件包含了“funcapi.h”这个头文件。

SQL函数中有一类需求是返回一个结果集。对于返回结果集的SQL函数的实现有一个固定的范式，可以参照src/include/funcapi.h的注释。

## 声明

PG中内置函数需要声明在src/include/utils/builtin.h（约定俗成的位置）。通常在该头文件中将实现的内置函数声明为一个extern函数。

```c++
extern Datum add_str(PG_FUNCTION_ARGS);
```

## 注册

与其他数据库对象相仿，内置函数的元信息必须写入PG的数据字典中。而且由于系统表是是数据库实例生来就有的对象，并没有一个CREATE能把元信息写入数据字典，因此这个步骤必须在源码中万冲。

内置函数的元数据是保存在系统表pg_proc中。而pg_proc系统表初始状态下的元数据是在src/include/catalog/pg_proc.h中注册的。每个元组的注册格式：

> DATA(insert OID = 元组的唯一OID ( 内置函数名 属性1 属性2…… ));
> DESCR(内置函数的描述信息(使用半角双引号引起来));

其中有两个地方需要注意

- OID必须保证全局唯一

  为pg_proc.h增加元组时，必须分配一个9999以内的唯一OID

- 元组各个字段的写法：pg_proc.h中的元数据的元组格式实际对应的是PostgreSQL手册中pg_proc系统表的各个字段

测试用例中元组可写为：

```c++
DATA(insert OID = 5946 (  add_str PGNSP PGUID 12 1 0 0 0 f f f f f f s s 2 0 25 "23 23" _null_ _null_ _null_ _null_ _null_ add_str _null_ _null_ _null_ ));
DESCR("add_str just for test");
```

### 问题：

按照上述的方法，初始化数据库时出现了如下的问题：

FATAL:array value must start with "{" or dimension information

经过搜索在PGSQL中是和函数定义有关的问题。因为openGauss和pgSQL毕竟是有区别的。

值得注意的是，openGauss的代码中含有**builtin_funcs.ini**这个文件，其中包含了所有的内置函数的一些信息。而无论是PGSQL的源码还是openGauss的源码在pg_proc.h中都没有注册函数的代码，但实际pg_proc表中却含有一些内置函数的信息。此外，builtin_funcs.ini实际上是记录了它的C函数名，也对应到了内置函数的定义方法。

上述方法是否可以实行？是否需要使用builtin_funcs.ini这个文件？另外还需要研究内置函数的调用方法。



[37.10. C 语言函数 (postgres.cn)](http://www.postgres.cn/docs/12/xfunc-c.html)

由fmgrtab.h可知，builtin_funcs.ini中的Builtin_func结构继承自FmgrBuiltin

```c++
typedef struct {
    Oid foid;             /* [ 0] */
    const char* funcName; /* [ 1] C name of the function or procedure name */
    int2 nargs;           /* [ 2] number of arguments, 0..FUNC_MAX_ARGS, or -1 if variable count */
    bool strict;          /* [ 3] strict with respect to NULLs? */
    bool retset;          /* [ 4] returns a set? */
    PGFunction func;      /* [ 5] pointer to compiled function */
    Oid rettype;          /* [ 6] OID of result type */
    Oid pronamespace;     /* [ 7] OID of namespace containing this proc */
    Oid proowner;         /* [ 8] procedure owner */
    Oid prolang;          /* [ 9] OID of pg_language entry, 12 for internal, 14 for SQL statement */
    float4 procost;       /* [10] estimated execution cost */
    float4 prorows;       /* [11] estimated # of rows out (if proretset) */
    Oid provariadic;      /* [12] element type of variadic array, or 0 */
    regproc protransform; /* [13] transforms calls to it during planning */
    bool proisagg;        /* [14] is it an aggregate? */
    bool proiswindow;     /* [15] is it a window function? */
    bool prosecdef;       /* [16] security definer */
    bool proleakproof;    /* [17] is it a leak-proof function? */
    char provolatile;     /* [18] see PROVOLATILE_ categories below */
    int2 pronargdefaults; /* [19] number of arguments with defaults */

    /* variable-length fields start here */
    ArrayOid proargtypes; /* [20] parameter types (excludes OUT params) */

    /* nullable fields start here, they are defined by pointer, if one of them
     * is null pointer, it means it has SQL NULL value */
    ArrayOid* proallargtypes;    /* [21] all param types (NULL if IN only) */
    ArrayChar* proargmodes;      /* [22] parameter modes (NULL if IN only) */
    ArrayCStr* proargnames;      /* [23] parameter names (NULL if no names) */
    const char* proargdefaults;  /* [24] list of expression trees for argument defaults (NULL if none) */
    const char* prosrc;          /* [25] procedure source text */
    const char* probin;          /* [26] secondary procedure info (can be NULL) */
    ArrayCStr* proconfig;        /* [27] procedure-local GUC settings */
    ArrayAcl* proacl;            /* [28] access permissions */
    ArrayInt2* prodefaultargpos; /* [29] */
    bool* fencedmode;            /* [30] */
    bool* proshippable;          /* [31] if provolatile is not 'i', proshippable will determine if the func can be shipped */
    bool* propackage;            /* [32] */
    const char* descr;           /* [33] description */
    char prokind;                /* [34] f:function, p:procedure*/
} Builtin_func;
```

### 后续

- 经过调试，builtin_funcs.ini中的函数确实是在SQL语句执行的时候被调用，可以尝试这种注册方法
